from model import gf

def utils(sentence):
    corrected_sentence = gf.correct(sentence, max_candidates=1)
    edited_sentence = gf.get_edits(sentence, corrected_sentence[0][0])
    return corrected_sentence[0][0], edited_sentence