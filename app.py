from flask import Flask, request, jsonify, render_template
from utils import utils

app = Flask(__name__)

@app.route("/", methods=['POST', 'GET'])
def home():
    if request.method == 'POST':
        original_sentence = request.form['sentence']
        corrected_sentence, edited_sentence = utils(original_sentence)
        return render_template('home.html', original_sentence=original_sentence, 
                                corrected_sentence=corrected_sentence,
                                edited_sentence=edited_sentence)
    else:
        return render_template('home.html')

# route for the api
@app.route("/api/check", methods=['POST'])
def getEdit():
    original_sentence = request.json.get("sentence")
    corrected_sentence, edited_sentence = utils(original_sentence)
    response = dict()
    for i in range(len(edited_sentence)):
        response[i] = dict()
    for i, text in enumerate(edited_sentence):
        response[i]["actual_word"] = text[1]
        response[i]["correct_word"] = text[4]
        response[i]["start_index"] = text[-2],
        response[i]["end_index"] = text[-1]
    return jsonify(response)

if __name__ == "__main__":
    app.run(host='localhost', port=8080)